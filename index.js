db.courses.insertMany([
    {
        name: "HTML Basics",
        price: 20000,
        isActive: true,
        instructor: "Sir Rome",
    },
    {
        name: "CSS 101 + Flexbox",
        price: 21000,
        isActive: true,
        instructor: "Sir Rome",
    },
    {
        name: "Javascript 101",
        price: 32000,
        isActive: true,
        instructor: "Ma'am Tine",
    },
    {
        name: "Git 101, IDE and CLI",
        price: 19000,
        isActive: false,
        instructor: "Ma'am Tine",
    },
    {
        name: "React.Js 101",
        price: 25000,
        isActive: true,
        instructor: "Ma'am Miah",
    },
]);

// Find courses whose instructor is "Sir Rome" and is priced greater than or equal to 20000
db.courses.find({
    $and: [
        {
            instructor: { $regex: "Sir Rome", $options: "$i" },
        },
        {
            price: { $gte: 20000 },
        },
    ],
});

// Find courses whose instructor is "Ma'am Tine"
// a.show only its name
db.courses.find({ instructor: "Ma'am Tine" }, { _id: 0, name: 1 });

//Find courses whose instructor is "Ma'am Miah"
// a. show only its name and price
db.courses.find({ instructor: "Ma'am Miah" }, { _id: 0, name: 1, price: 1 });

// Find courses with letter 'r' in its name and has a price greater than or equal to 20000
db.courses.find({
    $or: [
        {
            instructor: { $regex: "r", $options: "$i" },
        },
        {
            price: { $gte: 20000 },
        },
    ],
});

// Find courses which isActive field is true and has a price less than or equal to 25000
db.courses.find({
    $and: [
        {
            isActive: true,
        },
        {
            price: { $lte: 25000 },
        },
    ],
});
